package com.mrozon.tellmewhocall;

import android.app.Application;
import android.content.Context;

import com.mrozon.tellmewhocall.mnp.MnpApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyApplication extends Application {

    private static MnpApi mnpApi;
    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.megafon.ru")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mnpApi = retrofit.create(MnpApi.class);
    }

    public static MnpApi getMnpApi() {
        return mnpApi;
    }
}

