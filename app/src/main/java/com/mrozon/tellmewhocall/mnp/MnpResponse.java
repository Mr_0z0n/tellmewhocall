package com.mrozon.tellmewhocall.mnp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MnpResponse {

    @SerializedName("operator")
    @Expose
    private String operator;
    @SerializedName("operator_id")
    @Expose
    private Integer operatorId;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("region_id")
    @Expose
    private Integer regionId;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

}
