package com.mrozon.tellmewhocall.mnp;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MnpApi {

    //http://www.megafon.ru/api/mfn/info?msisdn=79267854252
    @GET("/api/mfn/info")
    Call<MnpResponse> getInfoByMssisdn(@Query("msisdn") String phone);

}
