package com.mrozon.tellmewhocall.mnp;

import android.support.annotation.NonNull;

import com.mrozon.tellmewhocall.MyApplication;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RxMnp {

    @NonNull
    public static Single<MnpResponse> getInfoByMssisdn(@NonNull String phone) {
        return Single.create(emitter -> MyApplication.getMnpApi().getInfoByMssisdn(phone)
                .enqueue(new Callback<MnpResponse>(){
          @Override
          public void onResponse(@NonNull Call<MnpResponse> call, @NonNull Response<MnpResponse> response) {
                emitter.onSuccess(response.body());
          }
          @Override
          public void onFailure(@NonNull Call<MnpResponse> call, @NonNull Throwable t) {
                emitter.onError(t);
          }
      }));
    }
}
