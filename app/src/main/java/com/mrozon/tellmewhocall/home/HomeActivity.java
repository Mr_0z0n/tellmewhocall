package com.mrozon.tellmewhocall.home;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.github.florent37.runtimepermission.rx.RxPermissions;
import com.mrozon.tellmewhocall.R;
import com.mrozon.tellmewhocall.settings.SettingsFragment;
import com.mrozon.tellmewhocall.utils.AppUtils;

import java.util.Objects;

import androidx.fragment.app.FragmentActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;


public class HomeActivity extends MvpAppCompatActivity {

    public final static int REQUEST_CODE = 10101;
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 10102;

    @BindView(R.id.contentFrame)
    FrameLayout frameLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.canDrawOverlays(this)) {

                // Launch service right away - the user has already previously granted permission
                //checkPhoneStatePermission();
                chechNeededPermissions();
            }
            else {

                // Check that the user has granted permission, and prompt them if not
                checkDrawOverlayPermission();
            }
        }
    }

    private void chechNeededPermissions() {
        askPermission(this,Manifest.permission.READ_PHONE_STATE)

                .request(Manifest.permission.READ_PHONE_STATE,Manifest.permission.READ_CALL_LOG)
                .subscribe();

    }

    public void checkDrawOverlayPermission() {
        // Checks if app already has permission to draw overlays
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                // If not, form up an Intent to launch the permission request
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                // Launch Intent, with the supplied request code
                startActivityForResult(intent, REQUEST_CODE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        // Check if a request code is received that matches that which we provided for the overlay draw request
        if (requestCode == REQUEST_CODE) {
            // Double-check that the user granted it, and didn't just dismiss the request
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    // Launch the service
                    checkPhoneStatePermission();
                }
                else {
                    showMessage( R.string.error_overlays_permission);
                }
            }
        }
    }

    private void showMessage(int res_id) {
        Toast.makeText(this, res_id, Toast.LENGTH_LONG).show();
    }

    private void checkPhoneStatePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_PHONE_STATE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                showMessage( R.string.explanation_read_phone_state);
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
            }
        } else {
            // Permission has already been granted
            showSettings();
        }
    }

    private void showSettings() {
        AppUtils.addFragmentToActivity(getSupportFragmentManager(),
                SettingsFragment.newInstance(), R.id.contentFrame, SettingsFragment.TAG);
    }
}
