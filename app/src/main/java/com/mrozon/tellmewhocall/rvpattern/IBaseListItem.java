package com.mrozon.tellmewhocall.rvpattern;

public interface IBaseListItem {
    int getLayoutId();
}
