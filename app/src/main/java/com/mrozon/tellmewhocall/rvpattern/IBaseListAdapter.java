package com.mrozon.tellmewhocall.rvpattern;

import java.util.List;

public interface IBaseListAdapter<T> {
    void add(T newItem);
    void add(List<T> newItems);
    void addAtPosition(int pos, T newItem);
    void remove(int position);
    void clearAll();
}
