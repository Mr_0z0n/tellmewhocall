package com.mrozon.tellmewhocall.rvpattern;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

public abstract class RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    implements IBaseListAdapter<IBaseListItem>{

    protected ArrayList<IBaseListItem> items = new ArrayList<>();
    protected RVAdapterItemClickListener listener;

    public interface RVAdapterItemClickListener {
        void onItemClick(IBaseListItem item);
        void onItemLongClick(IBaseListItem item);
    }

    public RVAdapter(RecyclerView recyclerView) {
//        SwipeController swipeController = new SwipeController(new SwipeControllerActions() {
//            @Override
//            public void onLeftClicked(int position) {
////                super.onLeftClicked(position);
//            }
//
//            @Override
//            public void onRightClicked(int position) {
////                super.onRightClicked(position);
//            }
//        });
//        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
//        itemTouchhelper.attachToRecyclerView(recyclerView);
//        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
//            @Override
//            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
//                swipeController.onDraw(c);
//            }
//        });
    }

    public void setListener(RVAdapterItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getLayoutId();
    }

    protected View inflateByViewType(Context context, int viewType, ViewGroup parent){
        return LayoutInflater.from(context).inflate(viewType, parent, false);
    }

    @Override
    public void add(IBaseListItem newItem) {
        items.add(newItem);
        notifyDataSetChanged();
    }

    @Override
    public void add(List<IBaseListItem> newItems) {
        for(IBaseListItem newItem: newItems){
            items.add(newItem);
            notifyDataSetChanged();
        }
    }

    @Override
    public void addAtPosition(int pos, IBaseListItem newItem) {
        items.add(pos, newItem);
        notifyDataSetChanged();
    }

    @Override
    public void remove(int position) {
        items.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public void clearAll() {
        items.clear();
        notifyDataSetChanged();
    }
}
