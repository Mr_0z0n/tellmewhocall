package com.mrozon.tellmewhocall.settings;

import android.app.Activity;
import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mrozon.tellmewhocall.PoppupWindow;

import static com.mrozon.tellmewhocall.utils.AppConstants.PADDING_BOTTOM_DEFAULT;
import static com.mrozon.tellmewhocall.utils.AppConstants.THREAD_DELAY;

@InjectViewState
public class SettingsPresenter extends MvpPresenter<SettingsView> {

    void showTestPoppup(Context context, Activity activity, int bottom){
        final PoppupWindow[] poppupWindow = {null};
        new Thread(()->{
            poppupWindow[0] = new PoppupWindow(context);
            poppupWindow[0].setBottomOffset(bottom);
            activity.runOnUiThread(()->poppupWindow[0].show(activity));
            try {
                Thread.sleep(THREAD_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            activity.runOnUiThread(()->poppupWindow[0].hide());
        }).start();
    }
}
