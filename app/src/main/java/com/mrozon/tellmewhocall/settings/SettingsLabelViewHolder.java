package com.mrozon.tellmewhocall.settings;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import com.mrozon.tellmewhocall.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsLabelViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_label_title)
    TextView tv_label_title;


    public SettingsLabelViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
