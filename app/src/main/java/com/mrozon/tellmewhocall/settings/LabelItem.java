package com.mrozon.tellmewhocall.settings;


import com.mrozon.tellmewhocall.R;
import com.mrozon.tellmewhocall.rvpattern.IBaseListItem;

public class LabelItem implements IBaseListItem {

    private String title;

    public LabelItem(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_settings_label;
    }
}
