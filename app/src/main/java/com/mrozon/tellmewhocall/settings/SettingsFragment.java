package com.mrozon.tellmewhocall.settings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.mrozon.tellmewhocall.PoppupWindow;
import com.mrozon.tellmewhocall.R;
import com.mrozon.tellmewhocall.mnp.RxMnp;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.mrozon.tellmewhocall.utils.AppConstants.PADDING_BOTTOM_DEFAULT;
import static com.mrozon.tellmewhocall.utils.AppConstants.WORK_AS_SERVICE_DEFAULT;


public class SettingsFragment extends MvpAppCompatFragment
        implements SettingsView{

    public static final String TAG = "SettingsFragment";

    public static final String WORK_AS_SERVICE = "work_as_service";
    private static final String PADDING_BOTTOM = "padding_bottom";

    private PoppupWindow poppupWindow=null;

    @InjectPresenter
    SettingsPresenter mPresenter;


    public static SettingsFragment newInstance() {
        Bundle args = new Bundle();
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.rvItems)
    RecyclerView mRecyclerView;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_indicator, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        if (id == R.id.menu_show_test_poppup){
                mPresenter.showTestPoppup(getContext(),getActivity(),
                        sharedPref.getInt(PADDING_BOTTOM,PADDING_BOTTOM_DEFAULT));
            Disposable subscribe = RxMnp.getInfoByMssisdn("79265690757").subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
//                    .doOnSubscribe(__ -> getViewState().showProgress())
                    .subscribe((response) -> {
//                                product = products.get(ProductTypes.IN_APP);
//                                getViewState().loadedSkus(product);
                                Toast.makeText(getActivity(), response.getOperator(), Toast.LENGTH_SHORT).show();
                            },
                            (throwable -> {
                                Toast.makeText(getActivity(), throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }));
            return  true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rv, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        //set title
        Objects.requireNonNull(((MvpAppCompatActivity) Objects.requireNonNull(getActivity())).
                getSupportActionBar()).setTitle(R.string.settings_fragment_title);

        //add adapter
        SettingsAdapter mAdapter = new SettingsAdapter(mRecyclerView, getActivity());
        mRecyclerView.setAdapter(mAdapter);

        //shared prefs
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        //settings
        mAdapter.add(new LabelItem(getString(R.string.settings_title_general)));
        mAdapter.add(new SwitchItem(getString(R.string.work_as_service), sharedPref.getBoolean(WORK_AS_SERVICE,WORK_AS_SERVICE_DEFAULT), WORK_AS_SERVICE));

        mAdapter.add(new LabelItem(getString(R.string.poppup_windows)));
        mAdapter.add(new EditTextItem(getString(R.string.padding_bottom),sharedPref.getInt(PADDING_BOTTOM,PADDING_BOTTOM_DEFAULT),PADDING_BOTTOM, PADDING_BOTTOM_DEFAULT));

        return view;
    }


}
