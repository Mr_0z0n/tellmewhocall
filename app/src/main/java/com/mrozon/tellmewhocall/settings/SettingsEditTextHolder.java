package com.mrozon.tellmewhocall.settings;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import com.mrozon.tellmewhocall.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsEditTextHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_ed_title)
    TextView tv_ed_title;

    @BindView(R.id.tv_ed_value)
    EditText tv_ed_value;


    public SettingsEditTextHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
