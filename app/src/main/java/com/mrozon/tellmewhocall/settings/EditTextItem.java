package com.mrozon.tellmewhocall.settings;


import com.mrozon.tellmewhocall.R;
import com.mrozon.tellmewhocall.rvpattern.IBaseListItem;

public class EditTextItem implements IBaseListItem {

    private String title;
    private int value;
    private String configName;
    private int defaultValue;

    public EditTextItem(String title, int value, String configName, int defaultValue) {
        this.title = title;
        this.value = value;
        this.configName = configName;
        this.defaultValue = defaultValue;
    }

    public String getConfigName() {
        return configName;
    }

    public int getDefaultValue() {
        return defaultValue;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_settings_edittext;
    }
}
