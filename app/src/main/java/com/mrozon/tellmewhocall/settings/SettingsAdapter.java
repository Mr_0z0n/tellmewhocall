package com.mrozon.tellmewhocall.settings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.mrozon.tellmewhocall.MyApplication;
import com.mrozon.tellmewhocall.R;
import com.mrozon.tellmewhocall.rvpattern.RVAdapter;


public class SettingsAdapter extends RVAdapter {

    private Activity activity;

    public SettingsAdapter(RecyclerView recyclerView, Activity activity) {
        super(recyclerView);
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        switch (i){
            case R.layout.item_settings_label:
                return new SettingsLabelViewHolder(inflateByViewType(context,i,viewGroup));
            case R.layout.item_settings_switch:
                return new SettingsSwitchViewHolder(inflateByViewType(context,i,viewGroup));
            case R.layout.item_settings_edittext:
                return new SettingsEditTextHolder(inflateByViewType(context,i,viewGroup));
            default:
                throw new IllegalStateException("There is no match with current layoutId");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if(viewHolder instanceof SettingsLabelViewHolder){
            LabelItem item = (LabelItem)items.get(i);
            SettingsLabelViewHolder settingsLabelViewHolder = (SettingsLabelViewHolder) viewHolder;
            settingsLabelViewHolder.tv_label_title.setText(item.getTitle());
        }
        if(viewHolder instanceof SettingsSwitchViewHolder){
            SwitchItem item = (SwitchItem)items.get(i);
            SettingsSwitchViewHolder settingsSwitchViewHolder = (SettingsSwitchViewHolder) viewHolder;
            settingsSwitchViewHolder.tv_switch_title.setText(item.getTitle());
            settingsSwitchViewHolder.tv_switch_value.setChecked(item.getSwitcher());
            settingsSwitchViewHolder.tv_switch_value.setOnCheckedChangeListener((compoundButton, b) -> {
                SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(item.getConfigName(), b);
                editor.apply();
            });
        }
        if(viewHolder instanceof SettingsEditTextHolder){
            EditTextItem item = (EditTextItem)items.get(i);
            SettingsEditTextHolder settingsEditTextHolder = (SettingsEditTextHolder) viewHolder;
            settingsEditTextHolder.tv_ed_title.setText(item.getTitle());
            settingsEditTextHolder.tv_ed_value.setText(String.valueOf(item.getValue()));
            settingsEditTextHolder.tv_ed_value.setOnFocusChangeListener((v, hasFocus) -> {
                if(v.getId() == R.id.tv_ed_value && !hasFocus) {
                    SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    try {
                        int inputValue = Integer.parseInt(settingsEditTextHolder.tv_ed_value.getText().toString());
                        editor.putInt(item.getConfigName(), inputValue);
                        editor.apply();
                    }
                    catch (NumberFormatException e){
                        settingsEditTextHolder.tv_ed_value.setText(String.valueOf(item.getDefaultValue()));
                        editor.putInt(item.getConfigName(), item.getDefaultValue());
                        editor.apply();
                    }

                    InputMethodManager imm =  (InputMethodManager) MyApplication.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if(imm!=null)
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                }
            });
        }
    }
}
