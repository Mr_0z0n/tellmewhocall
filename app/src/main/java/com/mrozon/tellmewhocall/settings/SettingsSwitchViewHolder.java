package com.mrozon.tellmewhocall.settings;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;


import com.mrozon.tellmewhocall.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsSwitchViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_switch_title)
    TextView tv_switch_title;

    @BindView(R.id.tv_switch_value)
    Switch tv_switch_value;


    public SettingsSwitchViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
