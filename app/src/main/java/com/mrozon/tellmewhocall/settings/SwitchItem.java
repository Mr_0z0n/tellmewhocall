package com.mrozon.tellmewhocall.settings;


import com.mrozon.tellmewhocall.R;
import com.mrozon.tellmewhocall.rvpattern.IBaseListItem;

public class SwitchItem implements IBaseListItem {

    private String title;
    private Boolean switcher;
    private String configName;

    public SwitchItem(String title, Boolean switcher, String configName) {
        this.title = title;
        this.switcher = switcher;
        this.configName = configName;
    }

    public String getConfigName() {
        return configName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getSwitcher() {
        return switcher;
    }

    public void setSwitcher(Boolean switcher) {
        this.switcher = switcher;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_settings_switch;
    }
}
