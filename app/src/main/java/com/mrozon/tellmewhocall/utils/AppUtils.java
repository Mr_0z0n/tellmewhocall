package com.mrozon.tellmewhocall.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import static com.mrozon.tellmewhocall.utils.AppConstants.APP_GOOGLE_PLAY_STORE_LINK;
import static com.mrozon.tellmewhocall.utils.AppConstants.APP_MARKET_LINK;

public class AppUtils {
    private AppUtils() {
        // This class is not publicly instantiable
    }

    public static void openPlayStoreForApp(Context context) {
        final String appPackageName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(APP_MARKET_LINK + appPackageName)));
        } catch (android.content.ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(APP_GOOGLE_PLAY_STORE_LINK + appPackageName)));
        }
    }

    public static void addFragmentToActivity (@NonNull FragmentManager fragmentManager,
                                              @NonNull Fragment fragment, int frameId, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.add(frameId, fragment, tag);
        transaction.replace(frameId, fragment, tag);
//        transaction.addToBackStack(tag);
        transaction.commit();

    }
}
