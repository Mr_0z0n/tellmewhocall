package com.mrozon.tellmewhocall.utils;

public class AppConstants {
    public static final String APP_MARKET_LINK = "market://details?id=";
    public static final String APP_GOOGLE_PLAY_STORE_LINK = "https://play.google.com/store/apps/details?id=";

    public static final int PADDING_BOTTOM_DEFAULT = 50;
    public static final boolean WORK_AS_SERVICE_DEFAULT = false;
    public static final int THREAD_DELAY = 3*1000;

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
