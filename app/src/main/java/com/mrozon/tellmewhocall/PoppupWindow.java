package com.mrozon.tellmewhocall;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.content.Context.WINDOW_SERVICE;

public class PoppupWindow {

    private Context context;
    private WindowManager wm;
    private WindowManager.LayoutParams params;
    private ConstraintLayout mOverlay;

    private int bottomOffset=140;

    public int getBottomOffset() {
        return bottomOffset;
    }

    public void setBottomOffset(int bottomOffset) {
        this.bottomOffset = bottomOffset;
    }

    public PoppupWindow(Context context) {
        this.context = context;
    }

    public void show(Activity activity){
        int LAYOUT_FLAG = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }


        wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        params = new WindowManager.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        //params.x = 10;
        params.y = bottomOffset;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        mOverlay = (ConstraintLayout) inflater.inflate(R.layout.poppup_window,null);
        wm.addView(mOverlay, params);
//        activity.runOnUiThread(() -> wm.addView(mOverlay, params));
    }


    public  void hide(){
        if(wm!=null)
            wm.removeView(mOverlay);
    }
}
